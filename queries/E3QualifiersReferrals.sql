SELECT
  NEWID() as ID,
  c.Email_Address as EmailAddress,
  c.First_Name,
  c.Last_Name,
  p.Customer_Id,
  p.Insurance_Product,
  p.Policy_Sales_Date,
  p.Ref_ID
FROM Policy_Data p
JOIN Customer_Data c
  ON p.Customer_Id = c.Customer_Id
WHERE 
  (
    p.Ref_ID IS NOT NULL
  AND
    p.Ref_ID != ''
  AND
    (
      p.Insurance_Product = 'ID Theft Policy'
      OR
      p.Insurance_Product = 'Multidevice'
    )
  )
  AND p.Policy_Sales_Date BETWEEN dateadd(day,datediff(day,0,getdate() - 31),0) 
  AND dateadd(day,datediff(day,0,getdate() - 30),0)