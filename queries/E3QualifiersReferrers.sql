SELECT
  NEWID() as ID,
  c.Email_Address as EmailAddress,
  p.First_Name,
  p.Last_Name,
  c.Customer_Id,
  p.Ref_ID,
  p.ID as Referral,
  p.Insurance_Product
FROM E3QualifiersReferrals p
JOIN Customer_Data c
  ON p.Ref_ID = c.Customer_Id