SELECT
  NEWID() as ID,
  c.Email_Address as EmailAddress,
  c.First_Name,
  c.Last_Name,
  p.Customer_Id,
  p.Insurance_Product,
  p.Ref_ID,
  p.Policy_Sales_Date
FROM Policy_Data p
JOIN Customer_Data c
  ON p.Customer_Id = c.Customer_Id
WHERE 
  (
  p.Insurance_Product = 'Car Rental'
  OR 
  p.Insurance_Product = 'Car Rental Roadside'
  OR 
  p.Insurance_Product = 'Car Rental RS'
  OR 
  p.Insurance_Product = 'Wireless'
  OR 
  p.Insurance_Product = 'Travel'
  OR 
  p.Insurance_Product = 'Travel Deluxe'
  OR 
  p.Insurance_Product = 'Rental'
  OR
  p.Insurance_Product = 'ID Theft Policy'
  OR 
  p.Insurance_Product = 'Gadget'
  )