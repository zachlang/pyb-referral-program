SELECT
  NEWID() as ID,
  c.Email_Address as EmailAddress,
  c.First_Name,
  c.Last_Name,
  p.Customer_Id,
  p.Insurance_Product,
  p.Ref_ID,
  p.Policy_Sales_Date
FROM Policy_Data p
JOIN Customer_Data c
  ON p.Customer_Id = c.Customer_Id
WHERE convert(varchar(8),p.Policy_Sales_Date,112) = convert(varchar(8),getdate()-1,112)
  AND 
  (
    p.Insurance_Product = 'ID Theft Policy'
    OR 
    p.Insurance_Product = 'Gadget'
  )